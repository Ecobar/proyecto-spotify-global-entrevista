# AngularSpotifyAPI

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.3.

# Para ejecutar

Por motivos de seguridad no puedo dejar mi cliente de spotify en el codigo, es necesario generar un token de manera manual que tendra una duracion de 1 hr, se puede ver esta guia https://developer.spotify.com/documentation/general/guides/app-settings/#register-your-app para registrar una aplicacion y obtener un client_id y un client_secret. con estos datos realizamos una peticion post (Puede usarse la aplicacion Postman) a la url https://accounts.spotify.com/api/token y enviamos en el body en modo "application/x-www-form-urlencoded" los siguientes elementos:

- grant_type: client_credentials (Esto va literal)
- client_id: cliente obtenido de spotify
- client_secret : cliente secret obtenido de spotify

Debe retornar en el body un token el cual pegamos en el archivo spotify.service.ts en la linea 20 reemplazando el existente sin borrar la plabra Bearer en la linea.

Realizado esto podemos compilar la aplicacion con los siguientes comandos (Se debe tener instalado node.js en el equipo)

-npm i
-npm start

Para ver la pagina principal nos metemos en un navegador con la url localhost:4200